import {ForbiddenException, Injectable} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {User} from "./entities/user.entity";
import {FindOptionsWhere, Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class UserService {
  constructor(
      @InjectRepository(User)
      private usersRepository: Repository<User>,
  ) {
  }
  async create(createUserDto: CreateUserDto) {

    const { username } = createUserDto;
    const usernameExists = await this.usersRepository.findOneBy({username});

    if (usernameExists) {
      throw new ForbiddenException("User already exists");
    }

    const newUser = {...new User(), ...createUserDto};

    try {
      await this.usersRepository.save(newUser);
    } catch (err) {
      throw new ForbiddenException("User already exists");
    }

    return newUser;
  }

  async findOne(id: number): Promise<User> {
    return this.findOneBy({id})
  }

  async findOneBy(by: FindOptionsWhere<User>): Promise<User> {
    const user = await this.usersRepository.findOneBy(by);

    if (!user) {
      throw new ForbiddenException("User not found");
    }
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOneBy({id});

    if (!user) {
      throw new ForbiddenException("User not found");
    }

    return this.usersRepository.save({...user, ...updateUserDto});
  }

  async remove(id: number) {
    return this.usersRepository.delete(id);
  }
}
