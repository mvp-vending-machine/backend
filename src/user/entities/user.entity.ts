import {ApiHideProperty, ApiPropertyOptional} from '@nestjs/swagger';

import * as bcrypt from 'bcrypt';
import {BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, PrimaryColumn,} from 'typeorm';
import {Exclude, instanceToPlain} from 'class-transformer';
import {RoleType} from "../types/role.type";

@Entity('users')
export class User extends BaseEntity {
    @PrimaryColumn()
    id: number;

    @Column({nullable: false, unique: true})
    username: string;

    @ApiHideProperty()
    @Exclude({toPlainOnly: true})
    @Column({nullable: false})
    password: string;

    @ApiHideProperty()
    @Exclude({toPlainOnly: true})
    @Column({nullable: false})
    salt: string;

    @Column({nullable: false})
    deposit: number;

    @ApiPropertyOptional()
    @Column({nullable: false})
    role: RoleType;

    @Column()
    created_at: string;

    @Column()
    updated_at: string;

    toJSON() {
        return instanceToPlain(this);
    }

    async validatePassword(password: string) {
        if (!this.password) {
            return false;
        }

        const hashedPassword = await bcrypt.hash(password, this.salt);

        return this.password === hashedPassword;
    }

    @BeforeInsert()
    async hashPassword(): Promise<void> {
        if (!!this.password) {
            this.salt = await bcrypt.genSalt();
            this.password = await bcrypt.hash(this.password, this.salt);
        }
    }

    @BeforeInsert()
    async assignDefaultRoleType(): Promise<void> {
        if (!this.role) {
            this.role = RoleType.BUYER;
        }
    }

    @BeforeInsert()
    async setCreatedTimestamp(): Promise<void> {
        this.created_at = (new Date()).toISOString();
    }

    @BeforeUpdate()
    async setUpdatedTimestamp(): Promise<void> {
        this.updated_at = (new Date()).toISOString();
    }
}
