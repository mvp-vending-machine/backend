export enum RoleType {
    SELLER = 'SELLER',
    BUYER = 'BUYER',
}