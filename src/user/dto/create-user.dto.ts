import {RoleType} from "../types/role.type";
import {IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString, MinLength} from "class-validator";
import {ApiPropertyOptional} from "@nestjs/swagger";

export class CreateUserDto {
    @IsString()
    @MinLength(5, {
        message: "Username is too short"
    })
    username: string;

    @IsString()
    @MinLength(6, {
        message: "Password is too short"
    })
    password: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsOptional()
    @IsEnum(RoleType)
    @ApiPropertyOptional()
    role: RoleType;
}
