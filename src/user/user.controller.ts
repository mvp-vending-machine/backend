import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  UseGuards,
  UsePipes,
  ValidationPipe
} from '@nestjs/common';
import {UserService} from './user.service';
import {CreateUserDto} from './dto/create-user.dto';
import {UpdateUserDto} from './dto/update-user.dto';
import {ApiOkResponse, ApiTags} from "@nestjs/swagger";
import {User} from "./entities/user.entity";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";

@ApiTags('User')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {
    }

    @ApiOkResponse({type: User, description: 'Create a new user.'})
    @UsePipes(ValidationPipe)
    @Post()
    create(@Body() createUserDto: CreateUserDto) {
        return this.userService.create(createUserDto);
    }

    @ApiOkResponse({type: User, description: 'Get user by auth token'})
    @Get()
    @UseGuards(JwtAuthGuard)
    findOne(@Param('id') id: string) {
        return this.userService.findOne(+id);
    }

    @ApiOkResponse({type: User, description: 'Update authenticated user'})
    @Put()
    @UseGuards(JwtAuthGuard)
    update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
        return this.userService.update(+id, updateUserDto);
    }

    @ApiOkResponse({type: User, description: 'Remove authenticated user'})
    @Delete()
    @UseGuards(JwtAuthGuard)
    remove(@Param('id') id: string) {
        return this.userService.remove(+id);
    }
}
