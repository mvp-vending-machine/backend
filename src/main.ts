import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ConfigService} from "@nestjs/config";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import {CorsOptions, CorsOptionsDelegate} from "@nestjs/common/interfaces/external/cors-options.interface";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const config = app.get(ConfigService);

    const corsOpts: CorsOptions | CorsOptionsDelegate<any> = {
        origin: (origin, callback) => {
            if (!origin) return callback(null, true);

            const whitelist = config.get('app.cors');

            if (whitelist.indexOf(origin) !== -1) {
                callback(null, true);
            } else {
                callback(new Error(`${origin} not allowed by CORS`));
            }
        },
        methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
        credentials: true,
    };
    app.enableCors(corsOpts);

    const options = new DocumentBuilder()
        .setTitle('Vending Machine API')
        .setVersion('1.0')
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api', app, document);

    await app.listen(config.get('app.port') as number);
}

bootstrap();
