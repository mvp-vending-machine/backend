export default () => ({
  cors: process.env.CORS_URL || ["127.0.0.1"],
  port: parseInt(process.env.PORT || "1234", 10),
});
