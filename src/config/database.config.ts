export default () => ({
    "type": "postgres",
    "host": process.env.POSTGRES_HOST,
    "username": process.env.POSTGRES_USER,
    "password": process.env.POSTGRES_PASSWORD,
    "database": process.env.POSTGRES_DB,
    "port": process.env.POSTGRES_PORT || 5432,
    "entities": ["dist/**/*.entity{.ts,.js}"],
    "synchronize": true,
    "ssl": true,
    "extra": {
        "ssl": {
            "rejectUnauthorized": false
        }
    }
});
