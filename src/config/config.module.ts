import {Module} from '@nestjs/common';
import {ConfigModule as NestConfigModule} from '@nestjs/config';
import AppConfig from './app.config';
import DatabaseConfig from './database.config';
import AuthConfig from './auth.config';

@Module({
    imports: [
        NestConfigModule.forRoot({
            isGlobal: true,
            load: [
                () => ({
                    app: AppConfig(),
                    auth: AuthConfig(),
                    database: DatabaseConfig(),
                }),
            ],
        }),
    ],
})
export class ConfigModule {
}
