import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ConfigModule} from "./config/config.module";
import {HealthController} from "./health/health.controller";
import {UserModule} from './user/user.module';
import {ProductModule} from './product/product.module';
import {SessionModule} from './session/session.module';
import {AuthModule} from "./auth/auth.module";
import {DatabaseModule} from "./database/database.module";

@Module({
    imports: [AuthModule, ConfigModule, DatabaseModule, UserModule, ProductModule, SessionModule],
    controllers: [AppController, HealthController],
    providers: [AppService],
})
export class AppModule {
}
