import {Module} from '@nestjs/common';
import {PassportModule} from '@nestjs/passport';

import {AuthService} from './auth.service';
import {AuthController} from './auth.controller';
import {LocalStrategy} from './strategies/local.strategy';
import {JwtStrategy} from './strategies/jwt.strategy';
import {UserModule} from "../user/user.module";
import {ConfigService} from "@nestjs/config";
import {JwtModule} from "@nestjs/jwt";

@Module({
    imports: [
        PassportModule.register({defaultStrategy: 'jwt'}),
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: async (config: ConfigService) => ({...config.get('auth.jwt')}),
        }),
        UserModule
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    controllers: [AuthController]
})

export class AuthModule {
}
