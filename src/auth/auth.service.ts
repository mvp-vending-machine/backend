import {Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';

import {UserService} from '../user/user.service';
import {User} from '../user/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findOneBy({username});


    if (user && await user.validatePassword(password)) {
      return user;
    }

    return null;
  }

  login(user: User) {
    const payload = { username: user.username, sub: user.id };
    return {
      jwt: this.jwtService.sign(payload),
      user,
    };
  }
}
