import {Body, Controller, HttpCode, Post, Request, UseGuards,} from '@nestjs/common';
import {ApiOkResponse, ApiTags} from '@nestjs/swagger';

import {AuthService} from './auth.service';
import {LocalAuthGuard} from './guards/local-auth.guard';
import {LoginDto, LoginResponseDto} from './dto';
import {AuthRequest} from "./types/auth.request.type";

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {
    }

    @ApiOkResponse({type: LoginResponseDto})
    @UseGuards(LocalAuthGuard)
    @Post('login')
    @HttpCode(200)
    login(@Body() loginDto: LoginDto, @Request() req: AuthRequest) {
        try {
            return this.authService.login(req.user);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
}
