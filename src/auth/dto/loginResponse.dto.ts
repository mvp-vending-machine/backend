import { User } from '../../user/entities/user.entity';

export class LoginResponseDto {
  jwt: string;
  user: User;
}
