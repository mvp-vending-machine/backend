FROM node:19-bullseye-slim AS builder
WORKDIR /app

ENV NODE_ENV=production

COPY ./ ./
RUN yarn global add @nestjs/cli && yarn install && yarn build

FROM node:19-bullseye-slim AS production
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist

ENV PORT=3000
EXPOSE 3000

CMD ["yarn", "start:prod"]